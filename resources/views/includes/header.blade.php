<header class="site-header header-one header-one__home-two">
    <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
        <div class="container clearfix">
            <div class="logo-box logo-box-left clearfix">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('assets/logo-csic.png') }}" class="main-logo" width="10%" alt="Awesome Image" />
                </a>
                <button class="menu-toggler" data-target=".main-navigation">
                    <span class="fa fa-bars"></span>
                </button>
            </div>
            <div class="main-navigation">
                <ul class="navigation-box one-page-scroll-menu ">
                    <li class="current scrollToLink">
                        <a href="#home">Home</a>
                    </li>
                    <li class="scrollToLink">
                        <a href="#events">Event</a>
                    </li>
                    <li class="scrollToLink">
                        <a href="#galleries">Gallery</a>
                    </li>
                    <li class="scrollToLink">
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <div class="right-side-box">
                <a href="https://tinyurl.com/RegistrasiCSIC2019" target="_blank" class="thm-btn header-one__btn">Register</a>
            </div>
        </div>
    </nav>
</header>