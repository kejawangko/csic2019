@extends('layouts.app')

@section('content')
<section class="banner-one" id="home">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-lg-5 col-md-6">
                <div class="banner-one__content">
                    <h3 class="banner-one__title">TAKE THE CHALLENGE</h3>
                    <h2 class="save-the-date" style="color: #ee4777">WIN THE PRIZE</h2>
                    <a href="#events" class="banner-one__btn thm-btn" style="float:right">Discover more</a>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<section class="brand-one" id="events">
    <div class="container">
        <div class="block-title text-center">
            <h2 class="block-title__title">ABOUT EVENT</h2>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center">
                <p> Computer Science Innovative Challenge merupakan kompetisi program kreativitas dan inovasi
                    mahasiswa pertama yang diselenggarakan oleh program studi Informatika Universitas
                    Multimedia Nusantara. Kegiatan ini merupakan inisiatif program studi Informatika UMN untuk
                    mendorong mahasiswa Indonesia untuk berinovasi dan memberikan apresiasi kepada insan-
                    insan muda penuh kreativitas yang telah berani untuk mempublikasikan ide-ide kreatif mereka
                    kepada khalayak luas.
                </p>
                <p>
                    Rangkaian kegiatan CSIC terdiri dari sosialisasi dan publikasi CSIC, pendaftaran, pengumpulan
                    purwarupa dan laporan, seleksi finalis, hingga akhirnya ditutup dengan presentasi dan
                    penjurian akhir, serta penutupan dan pengumuman pemenang.
                    Kompetisi ini terbuka bagi seluruh mahasiswa aktif di seluruh universitas se-Indonesia dengan
                    mengusung tema “Achieving Sustainable Development Goals 2030”<br><br>
                    Berikut adalah Rule Book CSIC 2019.
                </p>
                <a class="thm-btn" href="{{ asset('assets/documents/Rulebook CSIC 2019.pdf') }}" target="_blank">RULE BOOK</a>
                <br>
                <br>
                <br>
                <p>Adapun topik yang dapat dipilih mengenai </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="service-one__single">
                    <div class="service-one__icon">
                        <i class="fa fa-book"></i>
                    </div>
                    <h3 class="service-one__title"><a href="#">Quality Education</a></h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-one__single">
                    <div class="service-one__icon">
                        <i class="fa fa-building"></i>
                    </div>
                    <h3 class="service-one__title"><a href="#">Sustainable Cities and  <br> Communities</a></h3>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="service-one__single">
                    <div class="service-one__icon">
                        <i class="fa fa-tree"></i>
                    </div>
                    <h3 class="service-one__title"><a href="#">Life on Land</a></h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pricing-one" id="pricing">
    <div class="container">
        <div class="block-title text-center">
            <h2 class="block-title__title">GRAND PRIZE</h2>
        </div>
        <br>
        <div class="row">
            <div class="col-lg-4">
                <div class="pricing-one__single popular">
                    <div class="inner">
                        <div class="pricing-one__top">
                            <img src="{{ asset('assets/trophy_1.png') }}" width="30%" style="margin-bottom: 15px;">
                            <h4 class="pricing-one__amount">Rp 5.000.000,-</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="pricing-one__single popular">
                    <div class="inner">
                        <div class="pricing-one__top">
                            <img src="{{ asset('assets/trophy_2.png') }}" width="30%" style="margin-bottom: 15px;">
                            <h4 class="pricing-one__amount">Rp 3.000.000,-</h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="pricing-one__single popular">
                    <div class="inner">
                        <div class="pricing-one__top">
                            <img src="{{ asset('assets/trophy_3.png') }}" width="30%" style="margin-bottom: 15px;">
                            <h4 class="pricing-one__amount">Rp 1.000.000,-</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="funfact-one">
    <div class="container">
        <div class="block-title text-center">
            <h2 class="block-title__title">TIMELINE</h2>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <img src="{{ asset('assets/timeline.png') }}" width="100%">
            </div>
        </div>
    </div>
</section>
<section class="app-screen-one" id="galleries">
    <div class="container">
        <div class="block-title text-center">
            <h2 class="block-title__title">Galleries</h2>
        </div>
        <div class="app-screen-one__carousel owl-carousel owl-theme">
            @for ($i = 1; $i <= 22; $i++)
                <img src="{{ asset('assets/galleries/'.$i.'.jpg') }}" alt="Awesome Image" />
            @endfor
        </div>
    </div>
</section>
<section class="app-screen-one" id="sponsor">
    <div class="container">
        <div class="block-title text-center">
            <h2 class="block-title__title">Sponsors</h2>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-4 col-md-6 col-sm-6" style="padding-top: 25px;">
                <img src="{{ asset('assets/logo-codify.png') }}" width="100%">
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <img src="{{ asset('assets/logo-macroad.jpg') }}" width="50%">
            </div>
        </div>
    </div>
    {{-- <div style="border-bottom: 1px solid lightgrey; width: 75%; margin-right: auto; margin-left: auto;"></div> --}}
    <br><br><br>
    <div class="container">
        <div class="block-title text-center">
            <h2 class="block-title__title">Media Partners</h2>
        </div>
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                <img src="{{ asset('assets/logo-umn-radio.jpg') }}" width="25%" style="margin-right:25px">
                <img src="{{ asset('assets/logo-umn-tv.jpg') }}" width="25%" style="margin-right:25px">
                <img src="{{ asset('assets/logo-radio-untar.png') }}" width="25%">
            </div>
        </div>
    </div>
</section>
<footer class="site-footer" id="contact">
    <div class="site-footer__upper">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 text-center">
                    <div class="row">
                        <h2 class="block-title__title" style="color: #e75f61">Contact Person</h2><br>
                        <h3 class="footer-widget__text" style="color: black">
                            Alethea - 
                            021-5422 0808 (ext. 3312)
                        </h3>
                        <h3 class="footer-widget__text" style="color: black">
                            Kevin Christian - 
                            (ID Line) kevinchris1166
                        </h3>
                    </div>
                </div>
                <div class="col-lg-2"></div>
                <div class="col-lg-6">
                    <div class="map-area">
                        <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.0536621189462!2d106.61609431463829!3d-6.256661495471256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fc7b260ca1ed%3A0xb83752eec7c57ddd!2sUniversitas+Multimedia+Nusantara!5e0!3m2!1sen!2sid!4v1489216755656" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="site-footer__bottom">
        <div class="container">
            <div class="site-footer__social">
                {{-- <a href="#"><i class="fa fa-facebook-square"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-instagram"></i></a>
                <a href="#"><i class="fa fa-pinterest-p"></i></a> --}}
                {{-- <img src="{{ asset('assets/logo-codify.png') }}" width="10%"/> --}}
            </div>
            <p class="site-footer__copy-text"><i class="fa fa-copyright"></i>copyright 2019 by <a
                    href="#">Layerdrops.com</a></p>
        </div>
    </div>
</footer>
@endsection
